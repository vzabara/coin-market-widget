<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @copyright (c) phpBB Limited <https://www.phpbb.com>
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 * For full copyright and license information, please see
 * the docs/CREDITS.txt file.
 *
 */

namespace athc\coinmarketwidget\event;

/**
 * @ignore
 */
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener
 */
class widget_listener implements EventSubscriberInterface
{
	static public function getSubscribedEvents()
	{
		return array(
			'core.user_setup' => array(array('load_language_on_setup'), array('define_constants')),
			'core.page_header' => 'set_coin_id',
		);
	}

	/* @var \phpbb\controller\helper */
	protected $helper;

	/* @var \phpbb\template\template */
	protected $template;

	/* @var \phpbb\db\driver\driver_interface */
	protected $db;

	/**
	 * Constructor
	 *
	 * @param \phpbb\controller\helper $helper   Controller helper object
	 * @param \phpbb\template\template $template Template object
	 */
	public function __construct(\phpbb\controller\helper $helper, \phpbb\template\template $template, \phpbb\db\driver\driver_interface $db)
	{
		$this->helper = $helper;
		$this->template = $template;
		$this->db = $db;
	}

	public function define_constants()
	{
		include_once __DIR__ . '/../includes/constants.php';
	}

	/**
	 * Load the Acme Demo language file
	 *     acme/demo/language/en/demo.php
	 *
	 * @param \phpbb\event\data $event The event object
	 */
	public function load_language_on_setup($event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = array(
			'ext_name' => 'athc/coinmarketwidget',
			'lang_set' => 'coinmarketwidget',
		);
		$event['lang_set_ext'] = $lang_set_ext;
	}

	/**
	 * Set coin ID assigned to topic ID
	 */
	public function set_coin_id()
	{
		global $request;

        $parts = pathinfo($request->server('SCRIPT_NAME'));
        if (in_array($parts['basename'], $this->allowedPages())) {
		    $topic_id = $request->variable('t', 0);
			$sql = 'SELECT coin_id
            FROM ' . COINS_TOPICS_TABLE . '
            WHERE topic_id = "' . $this->db->sql_escape($topic_id) . '"';
			$result = $this->db->sql_query($sql);
			$row = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);
            if ($row['coin_id']) {
                $this->template->assign_var('COIN_ID', $row['coin_id']);
            }
        }
	}

    private function allowedPages()
    {
        return array(
            'viewtopic.php',
        );
    }
}
