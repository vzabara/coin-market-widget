<?php

namespace athc\coinmarketwidget\acp;

class widget_info
{
	public function module()
	{
		return array(
			'filename'  => '\athc\coinmarketwidget\acp\widget_module',
			'title'     => 'ACP_COINMARKET_TITLE',
			'modes'    => array(
				'settings'  => array(
					'title' => 'ACP_COINMARKET_SETTINGS',
					'auth'  => 'ext_athc/coinmarketwidget && acl_a_board',
					'cat'   => array('ACP_COINMARKET_TITLE'),
				),
			),
		);
	}
}