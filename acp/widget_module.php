<?php

namespace athc\coinmarketwidget\acp;

class widget_module
{
	var $u_action;

	function main($id, $mode)
	{
		global $db, $user, $template, $phpbb_log, $request;

		//$user->add_lang('coinmarketwidget');

		// Set up general vars
		$action = $request->variable('action', '');
		$action = (isset($_POST['add'])) ? 'add' : ((isset($_POST['save'])) ? 'save' : $action);

		$s_hidden_fields = '';
		$widget_info = array();

		$this->tpl_name = 'acp_coinmarketwidget_body';
		$this->page_title = 'ACP_COINMARKET_TITLE';

		$form_name = 'athc_coinmarketwidget_settings';
		add_form_key($form_name);

		switch ($action)
		{
			case 'edit':

				$widget_id = $request->variable('id', 0);

				if (!$widget_id)
				{
					trigger_error($user->lang['NO_WIDGET'] . adm_back_link($this->u_action), E_USER_WARNING);
				}

				$sql = 'SELECT ct.*, c.coin_name, t.topic_title
					FROM ' . COINS_TOPICS_TABLE . ' ct
					LEFT JOIN ' . COINS_TABLE . ' c ON c.coin_id=ct.coin_id
					LEFT JOIN ' . TOPICS_TABLE . ' t ON t.topic_id=ct.topic_id
					WHERE ct.id = '. $widget_id;
				$result = $db->sql_query($sql);
				$widget_info = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);

				$s_hidden_fields .= '<input type="hidden" name="id" value="' . $widget_id . '" />';

			case 'add':

				$sql1 = 'SELECT forum_id, forum_name
					FROM ' . FORUMS_TABLE . '
					ORDER by forum_id';
				$result1 = $db->sql_query($sql1);
				while ($row1 = $db->sql_fetchrow($result1)) {
					$template->assign_block_vars('forums', array(
						'FORUM_ID'			=> $row1['forum_id'],
						'FORUM_NAME'		=> $row1['forum_name'],
					));
					$sql2 = 'SELECT forum_id, topic_id, topic_title
					FROM ' . TOPICS_TABLE . '
					WHERE forum_id = ' . $row1['forum_id'] . '
					ORDER by topic_id';
					$result2 = $db->sql_query($sql2);
					while ($row2 = $db->sql_fetchrow($result2)) {
						$template->assign_block_vars('forums.topics', array(
							'TOPIC_ID'			=> $row2['topic_id'],
							'TOPIC_TITLE'		=> $row2['topic_title'],
							'TOPIC_SELECTED'	=> $widget_info['topic_id'],
						));
					}
					$db->sql_freeresult($result2);
				}
				$db->sql_freeresult($result1);
/*
				$sql2 = 'SELECT forum_id, topic_id, topic_title
					FROM ' . TOPICS_TABLE . '
					WHERE forum_id = ' .  . '
					ORDER by topic_id';
				$result = $db->sql_query($sql);
				while ($row = $db->sql_fetchrow($result)) {
					$template->assign_block_vars('topics', array(
						'FORUM_ID'			=> $row['forum_id'],
						'TOPIC_ID'			=> $row['topic_id'],
						'TOPIC_TITLE'		=> $row['topic_title'],
						'TOPIC_SELECTED'	=> $widget_info['topic_id'],
					));
				}
				$db->sql_freeresult($result);
*/
				$sql = 'SELECT *
					FROM ' . COINS_TABLE . '
					ORDER by coin_name';
				$result = $db->sql_query($sql);
				while ($row = $db->sql_fetchrow($result)) {
					$template->assign_block_vars('coins', array(
						'COIN_ID'			=> $row['coin_id'],
						'COIN_NAME'			=> $row['coin_name'],
						'COIN_SELECTED'		=> $widget_info['coin_id'],
					));
				}
				$db->sql_freeresult($result);

				$template->assign_vars(array(
						'S_EDIT_WORD'		=> true,
						'U_ACTION'			=> $this->u_action,
						'U_BACK'			=> $this->u_action,
						'COIN_ID'			=> (isset($widget_info['coin_id'])) ? $widget_info['coin_id'] : '',
						'COIN_NAME'			=> (isset($widget_info['coin_name'])) ? $widget_info['coin_name'] : '',
						'TOPIC_ID'			=> (isset($widget_info['topic_id'])) ? $widget_info['topic_id'] : '',
						'TOPIC_TITLE'		=> (isset($widget_info['topic_title'])) ? $widget_info['topic_title'] : '',
						'S_HIDDEN_FIELDS'	=> $s_hidden_fields)
				);

				return;

			break;

			case 'save':

				if (!check_form_key($form_name))
				{
					trigger_error($user->lang['FORM_INVALID']. adm_back_link($this->u_action), E_USER_WARNING);
				}

				$widget_id		= $request->variable('id', 0);
				$coin_id		= $request->variable('coin', '', true);
				$topic_id		= $request->variable('topic', '', true);

				if ($coin_id === '' || $topic_id === '')
				{
					trigger_error($user->lang['ENTER_DATA'] . adm_back_link($this->u_action), E_USER_WARNING);
				}

				$sql_ary = array(
					'coin_id'	=> $coin_id,
					'topic_id'	=> $topic_id
				);

				if ($widget_id)
				{
					$db->sql_query('UPDATE ' . COINS_TOPICS_TABLE . ' SET ' . $db->sql_build_array('UPDATE', $sql_ary) . ' WHERE id = ' . $widget_id);
				}
				else
				{
					$db->sql_query('INSERT INTO ' . COINS_TOPICS_TABLE . ' ' . $db->sql_build_array('INSERT', $sql_ary));
				}

				$log_action = ($widget_id) ? 'LOG_COIN_WIDGET_EDIT' : 'LOG_COIN_WIDGET_ADD';

				$phpbb_log->add('admin', $user->data['user_id'], $user->ip, $log_action, false, array($widget_id));

				$message = ($widget_id) ? $user->lang['WIDGET_UPDATED'] : $user->lang['WIDGET_ADDED'];
				trigger_error($message . adm_back_link($this->u_action));

			break;

			case 'delete':

				$widget_id = $request->variable('id', 0);

				if (!$widget_id)
				{
					trigger_error($user->lang['NO_WIDGET'] . adm_back_link($this->u_action), E_USER_WARNING);
				}

				if (confirm_box(true))
				{
					$sql = 'DELETE FROM ' . COINS_TOPICS_TABLE . "
						WHERE id = $widget_id";
					$db->sql_query($sql);

					$phpbb_log->add('admin', $user->data['user_id'], $user->ip, 'LOG_COIN_WIDGET_DELETE', false, array($widget_id));

					trigger_error($user->lang['WIDGET_REMOVED'] . adm_back_link($this->u_action));
				}
				else
				{
					confirm_box(false, $user->lang['CONFIRM_OPERATION'], build_hidden_fields(array(
						'i'			=> $id,
						'mode'		=> $mode,
						'id'		=> $widget_id,
						'action'	=> 'delete',
					)));
				}

			break;
		}

		$template->assign_vars(array(
				'U_ACTION'			=> $this->u_action,
				'S_HIDDEN_FIELDS'	=> $s_hidden_fields)
		);


		$sql = 'SELECT ct.*, c.coin_name, t.topic_title, f.forum_name
					FROM ' . COINS_TOPICS_TABLE . ' ct
					LEFT JOIN ' . COINS_TABLE . ' c ON c.coin_id=ct.coin_id
					LEFT JOIN ' . TOPICS_TABLE . ' t ON t.topic_id=ct.topic_id
					LEFT JOIN ' . FORUMS_TABLE . ' f ON f.forum_id=t.forum_id';
		$result = $db->sql_query($sql);

		while ($row = $db->sql_fetchrow($result))
		{
			$template->assign_block_vars('widgets', array(
					'FORUM_NAME'	=> $row['forum_name'],
					'TOPIC_TITLE'	=> $row['topic_title'],
					'COIN_NAME'		=> $row['coin_name'],
					'U_EDIT'		=> $this->u_action . '&amp;action=edit&amp;id=' . $row['id'],
					'U_DELETE'		=> $this->u_action . '&amp;action=delete&amp;id=' . $row['id'])
			);
		}
		$db->sql_freeresult($result);
	}
}