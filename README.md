## Cryptocurrency Price Ticker Widget for phpBB ##

The widget allows to show Cryptocurrency Price Ticker Widget on top of forum topics.

![ACP](screenshot-3.png)

![ACP](screenshot-1.png)

![Topic](screenshot-2.png)
See https://coinmarketcap.com/widget/
